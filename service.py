from datetime import datetime

from trytond.pool import Pool, PoolMeta
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, Button, StateTransition, StateView


class LabTestRequestForm(ModelView):
    'Lab Test Request Form'
    __name__ = 'health_lab.service_lab_test_request.form'
    patient = fields.Many2One('health.patient', 'Patient', required=True)
    tests = fields.Many2Many('health.lab.test_type', None, None, 'Test type',
        required=True)


class LabTestRequest(Wizard):
    'Service Lab Test Request'
    __name__ = 'health_lab.service_lab_test_request'
    start = StateView('health_lab.service_lab_test_request.form',
        'health_lab.service_lab_test_request_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'accept', 'tryton-ok', default=True),
        ])
    accept = StateTransition()

    def transition_accept(self):
        pool = Pool()
        LabTest = pool.get('health.lab_test')
        Service = pool.get('health.service')
        ServiceLine = pool.get('health.service.line')
        now = datetime.now()
        products = []
        labs = []
        desc = 'Lab. Test'
        for test in self.start.tests:
            plan = None
            insurances = self.start.patient.party.insurance
            if insurances:
                plan = insurances[0].plan

            unit_price = ServiceLine.compute_price(test.product, plan)
            lab_test, = LabTest.create([{
                'patient': self.start.patient.id,
                'request_date': now,
                'priority': 'medium',
                'kind': 'external',
                'test': test.id,
            }])
            labs.append(lab_test)
            products.append({
                'product': test.product.id,
                'unit_price': unit_price,
                'qty': 1,
                'to_invoice': True,
                'state': 'draft',
                'desc': test.rec_name,
                'origin': str(lab_test),
            })
            desc += ' | ' + lab_test.number

        to_create = {
            'patient': self.start.patient.id,
            'invoice_to': self.start.patient.party.id,
            'desc': desc,
            'state': 'draft',
            'lines': [('create', products)],
        }
        if plan:
            to_create['insurance_plan'] = plan.id
        service, = Service.create([to_create])
        LabTest.write(labs, {'origin': str(service)})
        return 'end'


class HealthServiceLine(metaclass=PoolMeta):
    __name__ = 'health.service.line'

    @classmethod
    def _get_origin(cls):
        return super(HealthServiceLine, cls)._get_origin() + [
            'health.lab_test'
        ]
