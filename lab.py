from datetime import datetime
from trytond.model import ModelView, ModelSQL, fields, Unique, Workflow
from trytond.pool import Pool
from trytond.pyson import Eval, Bool


class TestType(ModelSQL, ModelView):
    'Type of Lab test'
    __name__ = 'health.lab.test_type'
    active = fields.Boolean('Active', select=True)
    name = fields.Char('Test', select=True, translate=True,
        help="Test type, eg X-Ray, hemogram,biopsy...", required=True)
    code = fields.Char('Code', required=True, select=True,
        help="Short name - code for the test")
    info = fields.Text('Description')
    product = fields.Many2One('product.product', 'Product')
    critearea = fields.One2Many('health.lab.test.critearea', 'test_type',
        'Test Cases')
    kind = fields.Selection([
        ('internal', 'Internal'),
        ('external', 'External'),
        ], 'Kind', select=True)

    @staticmethod
    def default_kind():
        return 'internal'

    @staticmethod
    def default_active():
        return True

    @classmethod
    def __setup__(cls):
        super(TestType, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [
            ('code_uniq', Unique(t, t.name),
             'The Lab Test code must be unique')
        ]

    @classmethod
    def check_xml_record(cls, records, values):
        return True

    @classmethod
    def search_rec_name(cls, name, clause):
        """ Search for the full name and the code """
        field = None
        for field in ('name', 'code'):
            tests = cls.search([(field,) + tuple(clause[1:])], limit=1)
            if tests:
                break
        if tests:
            return [(field,) + tuple(clause[1:])]
        return [(cls._rec_name,) + tuple(clause[1:])]


class LabTest(Workflow, ModelSQL, ModelView):
    'Laboratory Test'
    __name__ = 'health.lab_test'
    _rec_name = 'number'
    STATES_VALIDATED = {
        'readonly': Eval('state').in_(['validated', 'cancelled'])
    }

    number = fields.Char('Order Number', help="Number ID", readonly=True)
    patient = fields.Many2One('health.patient', 'Patient', required=True,
        states={
            'readonly': Bool(Eval('origin'))
        }, select=True)
    test = fields.Many2One('health.lab.test_type', 'Test type',
        help="Lab test type", required=True, select=True, states={
            'readonly': Eval('state') != 'draft',
        })
    pathologist = fields.Many2One('health.professional', 'Pathologist',
        states={
            'readonly': Eval('state').in_(['validated', 'cancelled']),
            'invisible': Eval('state').in_(['draft', 'ordered'])
        }, help="Bioanalyst who validate the test")
    physician = fields.Many2One('health.professional', 'Physician',
        help="Doctor who requested the test", select=True, states={
            'readonly': True,
        })
    request_date = fields.DateTime('Request Date', readonly=True,
        required=True)
    analysis_date = fields.DateTime('Analysis Date', select=True,
        states={'readonly': True})
    origin = fields.Reference('Origin', selection='get_origin', select=True,
        states={'readonly': Eval('state') != 'draft'}, depends=['state'])
    state = fields.Selection([
        ('draft', 'Draft'),
        ('ordered', 'Ordered'),
        ('in_process', 'In Process'),
        ('tested', 'Tested'),
        ('validated', 'Validated'),
        ('cancelled', 'Cancelled'),
        ], 'State', readonly=True, select=True)
    priority = fields.Selection([
        ('urgent', 'Urgent'),
        ('medium', 'Medium'),
        ('low', 'Low'),
        ], 'Priority', select=True, states={
            'readonly': Eval('state') != 'draft'
    })
    kind = fields.Selection([
        ('internal', 'Internal'),
        ('external', 'External'),
        ], 'Kind', select=True, states={
            'readonly': Eval('state').in_([
                'ordered', 'cancelled', 'validated'
            ])
        })
    lines = fields.One2Many('health.lab_test.line',
        'lab_test', 'Lines', states={
            'readonly': Eval('state').in_(['draft', 'cancelled', 'validated']),
            'invisible': Eval('state').in_(['draft', 'ordered'])
        })
    results = fields.Text('Results', states=STATES_VALIDATED)
    presuntive_diagnosis = fields.Many2One('health.pathology',
        'Presuntive Diagnosis', states={
            'readonly': Eval('state') != 'draft',
        }
    )
    diagnosis = fields.Text('Diagnosis', states={
        'readonly': Eval('state') != 'draft',
    })

    @classmethod
    def __setup__(cls):
        super(LabTest, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [(
            'id_uniq', Unique(t, t.number), 'The test ID code must be unique'
        )]
        cls._order.insert(0, ('request_date', 'DESC'))
        cls._order.insert(1, ('number', 'DESC'))
        cls._order.insert(2, ('test', 'ASC'))
        cls._buttons.update({
            'draft': {
                'invisible': Eval('state') != 'ordered',
            },
            'request': {
                'invisible': Eval('state') != 'draft',
            },
            'in_process': {
                'invisible': Eval('state') != 'ordered'
            },
            'tested': {
                'invisible': Eval('state') != 'in_process',
            },
            'validate': {
                'invisible': Eval('state') != 'tested',
            },
            'cancel': {
                'invisible': Eval('state') != 'draft',
            },
        })
        cls._transitions |= set((
            ('draft', 'ordered'),
            ('ordered', 'in_process'),
            ('ordered', 'cancelled'),
            ('in_process', 'tested'),
            ('tested', 'validated'),
            ('cancelled', 'draft'),
        ))

    @classmethod
    def _get_origin(cls):
        'Return list of Model names for origin Reference'
        return ['health.service', 'health.patient.evaluation']

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        get_name = Model.get_name
        models = cls._get_origin()
        return [(None, '')] + [(m, get_name(m)) for m in models]

    @staticmethod
    def default_request_date():
        return datetime.now()

    @staticmethod
    def default_kind():
        return 'internal'

    @staticmethod
    def default_priority():
        return 'medium'

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_physician():
        HealthProf = Pool().get('health.professional')
        try:
            return HealthProf.get_health_professional()
        except:
            pass

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        config = pool.get('health.configuration').get_config()
        TestType = pool.get('health.lab.test_type')
        vlist = [x.copy() for x in vlist]
        for val in vlist:
            lines = []
            test_type = TestType(val['test'])
            for c in test_type.critearea:
                lines.append({
                    'critearea': c.id,
                    'lower_limit': c.lower_limit,
                    'upper_limit': c.upper_limit,
                    'units': c.units.id,
                    'normal_range': c.normal_range,
                })
            val['number'] = config.lab_request_sequence.get()
            val['lines'] = [('create', lines)]
            val['state'] = 'ordered'
        return super(LabTest, cls).create(vlist)

    @classmethod
    def copy(cls, tests, default=None):
        if default is None:
            default = {}
        default = default.copy()
        default['number'] = None
        default['date'] = cls.default_date()
        return super(LabTest, cls).copy(tests, default=default)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    def draft(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('ordered')
    def request(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('in_process')
    def in_process(cls, records):
        cls.write(records, {'analysis_date': datetime.now()})

    @classmethod
    @ModelView.button
    @Workflow.transition('tested')
    def tested(cls, records):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('validated')
    def validate(cls, records):
        pass


class HealthLabTestUnits(ModelSQL, ModelView):
    'Lab Test Units'
    __name__ = 'health.lab.test.units'

    name = fields.Char('Unit', select=True)
    code = fields.Char('Code', select=True)

    @classmethod
    def __setup__(cls):
        super(HealthLabTestUnits, cls).__setup__()
        t = cls.__table__()
        cls._sql_constraints = [(
            'name_uniq', Unique(t, t.name), 'The Unit name must be unique'
        )]

    @classmethod
    def check_xml_record(cls, records, values):
        return True


class HealthTestCritearea(ModelSQL, ModelView):
    'Lab Test Critearea'
    __name__ = 'health.lab.test.critearea'

    name = fields.Char('Analyte', required=True, select=True, translate=True)
    normal_range = fields.Text('Reference')
    lower_limit = fields.Float('Lower Limit')
    upper_limit = fields.Float('Upper Limit')
    test_type = fields.Many2One('health.lab.test_type', 'Test type', select=True)
    warning = fields.Boolean('Warn', help='Warns the patient about this '
        ' analyte result'
        ' It is useful to contextualize the result to each patient status '
        ' like age, sex, comorbidities, ...')
    units = fields.Many2One('health.lab.test.units', 'Units')
    sequence = fields.Integer('Sequence')
    excluded = fields.Boolean('Excluded', help='Select this option when'
        ' this analyte is excluded from the test')

    @classmethod
    def __setup__(cls):
        super(HealthTestCritearea, cls).__setup__()
        cls._order.insert(0, ('sequence', 'ASC'))

    @staticmethod
    def default_sequence():
        return 1

    @staticmethod
    def default_excluded():
        return False


class LabTestLine(ModelSQL, ModelView):
    'Lab Test Line'
    __name__ = 'health.lab_test.line'
    lab_test = fields.Many2One('health.lab_test', 'Lab Test', required=True)
    critearea = fields.Many2One('health.lab.test.critearea', 'Critearea',
        select=True)
    excluded = fields.Boolean('Excluded', help='Select this option when'
        ' this analyte is excluded from the test')
    result = fields.Float('Value')
    result_text = fields.Char('Result - Text', help='Non-numeric results. For '
        'example qualitative values, morphological, colors ...')
    remarks = fields.Char('Remarks')
    normal_range = fields.Text('Reference')
    lower_limit = fields.Float('Lower Limit')
    upper_limit = fields.Float('Upper Limit')
    warning = fields.Boolean('Warn', help='Warns the patient about this '
        ' analyte result'
        ' It is useful to contextualize the result to each patient status '
        ' like age, sex, comorbidities, ...')
    units = fields.Many2One('health.lab.test.units', 'Units')
    sequence = fields.Integer('Sequence')
    # Show the warning icon if warning is active on the analyte line
    lab_warning_icon = fields.Function(fields.Char('Lab Warning Icon'),
         'get_lab_warning_icon')
    remarks = fields.Char('Remarks')

    @fields.depends('result', 'lower_limit', 'upper_limit')
    def on_change_with_warning(self):
        if (self.result < self.lower_limit or self.result > self.upper_limit):
            return True
        return False

    @classmethod
    def check_xml_record(cls, records, values):
        return True

    def get_lab_warning_icon(self, name):
        if self.warning:
            return 'health-warning'
