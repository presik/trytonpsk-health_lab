
from trytond.pool import Pool
from . import lab
from . import configuration
from . import service
from . import evaluation
from .report import labtest
from .wizard import wizard_create_lab_test


def register():
    Pool.register(
        configuration.HealthConfiguration,
        lab.TestType,
        lab.LabTest,
        lab.LabTestLine,
        lab.HealthLabTestUnits,
        lab.HealthTestCritearea,
        service.HealthServiceLine,
        service.LabTestRequestForm,
        evaluation.PatientEvaluation,
        wizard_create_lab_test.RequestTest,
        wizard_create_lab_test.RequestPatientLabTestStart,
        wizard_create_lab_test.CreateLabTestOrderInit,
        module='health_lab', type_='model')
    Pool.register(
        wizard_create_lab_test.CreateLabTestOrder,
        wizard_create_lab_test.RequestPatientLabTest,
        service.LabTestRequest,
        module='health_lab', type_='wizard')
    Pool.register(
        labtest.LabTestReport,
        module='health_lab', type_='report')
